# Basketball Players project

Look into the directory `data_files` for a description of the project.

## Required

- Tested on Ubuntu 14.04. I hope it works on other distributions of linux. It is not tested on any other OS.
- Point cloud library [PCL](http://pointclouds.org), tested with V. 1.7
- Eigen library

## Build

- I compiled the package in my ROS catkin environment, but building does not depend on that, feel free to uncomment the catkin part in CMakeLists.

- You should be able to just do 

    - `mkdir build && cd build`
    - `cmake -DCMAKE_BUILD_TYPE=Release ..`
    - `./assignment`

- The release build type is used because of a known issue in PCL 1.7. If you are using higher versions, probably you need 

## How it works

- Cloud gets loaded from `data_files` directory. If you do not build from `build` directory, change `dir_path` in `basket_players_assignment.cpp`

- Performs Euclidean cluster extraction from the loaded point cloud. Some parameters can be adjusted. Default parameters are for the given example. In ideal situation each resulting cluster should contain one and only one person.

- Throws away the extracted clusters which are too small. The clusters which are too large need to be reclustered again. In ideal situation each cluster should contain one and only one person. 

- *In this example, two people are too close to each other and there is noise in the space between them. As a result cluster extraction is not straightforward. So this goes to todo list.*

- Categorizes the extracted clusters into different teams.

    - Categorization currently separates one cluster from another based on the color intensity (*which is calculated by RGB average*) of the average color of each cluster. So first calculates the cluster average RGB and then averages over the R & G & B to get intensity.

    - In order to improve the results, color averaging of each cluster is only done on a part of the players body covered with clothes, this means that the legs and head are filtered before color averaging.


## Todo

- Find a better way for segmentation so that the players can be separated from each other in the large cluster 