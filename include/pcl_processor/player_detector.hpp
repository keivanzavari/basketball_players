#include <iostream>
#include <vector>
#include <string>
#include <fstream>

#include <Eigen/Core>

#include <pcl/io/pcd_io.h>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include <pcl/common/centroid.h>

#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>

#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>

#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/cloud_viewer.h>

#include "color.h"

/**
* In this class all points are of XYZ RGB type and contain color information
*/
typedef pcl::PointXYZRGB PointType;
typedef pcl::PointCloud<PointType> CloudType;
typedef boost::shared_ptr<pcl::visualization::PCLVisualizer> VisualizerPtr;
const int POINTELEMENTS = 6;


class PlayerDetector {

public:
	/**
	* Initializes variables
	*/
	PlayerDetector();

	/**
	* Loads a pointcloud with XYZ RGB points from a text file
	*/
	bool loadCloudXYZRGB (const std::string & file_path);
	
	/**
	* Saves a point cloud to a PCD file
	*/
	int saveCloudPCD (const std::string & file_path);
	
	/**
	* Loads a point cloud from a PCD file
	*/
	bool loadCloudPCD (const std::string & file_path);


	/**
	* Performs Euclidean cluster extraction from a point cloud
	* some parameters can be adjusted
	* default parameters are for the given example
	*
	* In ideal situation each cluster should contain one and only one person
	* 
	*/
	int clusterExtraction(float tolerance = 0.1, int min_cluster_size=50,
                           int max_cluster_size=1e4);

	/**
	* Throws away the extracted clusters which are too small
	* and re-clusters the clouds which are too large
	*
	* In ideal situation each cluster should contain one and only one person
	* If two people are too close to each other and there is noise in the space 
	* between them, cluster extraction will not be very straightforward
	*/
	int removeNoiseFromCluster();

	/**
	* Categorizes the extracted clusters into different teams
	*
	* This function currently separates one cluster from another based on
	* color intensity (which is calculated by RGB average) of the average color 
	* of each cluster
	*
	* In order to improve the results, color averaging of each cluster is only
	* done on a part of the players body covered with clothes, this means that the
	* legs and head are filtered before color averaging 
	*/
	int categorize();

	/**
	* Get the location of each found person (team A or B or referee). 
	* calculated location is the centroid of the cluster
	*/
	void getLocations();


	/**
	* Prints the locations
	*/
	void printLocations();

	/**
	* return the pointer to a new viewer to show a colored point cloud where each 
	* group is in a different color
	*/
	VisualizerPtr viewCategorizedCloud ();

	/**
	* Enable and disable print statements, useful for debugging
	*/
	void setDebug(bool debug);

private:

	/**
	* A simple passthrough filter to filter the legs and the head of each person 
	*/
	void passThroughFilter(const CloudType::Ptr & cloud, 
                       CloudType::Ptr & cloud_filtered,
                       const int min, const int max, const bool negative);

	/**
	* Get the average color of a cloud, this function uses the passThroughFilter
	*/
	void getPlayerColor(const CloudType::Ptr & cloud, Eigen::Vector3i & rgb);

	/**
	* Calculates the average color of a cloud
	*/
	void getColorAverage(const CloudType::Ptr & cloud, Eigen::Vector3i & rgb);


	/**
	* set the color of the cloud to a given color, only used for visualization
	*/
	void changeCloudColor(CloudType::Ptr & cloud, const Eigen::Vector3i & rgb);

	/**************************
	*
	* Variables
	*
	**************************/
	bool debug_;

	pcl::PCDWriter pcd_writer_;

	int min_person_height_;
	int max_person_height_;
  
	int average_nr_points_per_person_;
	int expected_nr_people_;

	bool cloud_loaded_;
	int original_cloud_size_;
	/**
	* original cloud which gets loaded when load functions are called and 
	* gets saved when save method is called
	*/
	CloudType::Ptr cloud_;

	bool save_clusters_;
	/**
	* the vector of point clouds containing the clusters coming from 
	* Euclidean cluster extraction. This cluster gets modified by categorize method
	*/
	std::vector<CloudType::Ptr> cloud_clustered_;
	std::vector<pcl::PointIndices> cluster_indices_;


	std::vector<int> cluster_indices_team_a_;
	std::vector<int> cluster_indices_team_b_;
	std::vector<int> cluster_indices_referee_;

	std::vector<Eigen::Matrix < float, 2, 1 > > locations_team_a_;
	std::vector<Eigen::Matrix < float, 2, 1 > > locations_team_b_;
	std::vector<Eigen::Matrix < float, 2, 1 > > locations_referee_;
	
};