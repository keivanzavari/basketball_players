# if you get seg fault, it's some wierd boost/pcl 1.7 issue, 
# try building Release type:
# cmake -DCMAKE_BUILD_TYPE=Release
# OR 
# catkin_make -DCMAKE_BUILD_TYPE=Release


cmake_minimum_required(VERSION 2.8.3)
project(pcl_processor)

# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DBT_USE_DOUBLE_PRECISION -Wall")
# # Unused warnings
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wuninitialized -Winit-self -Wunused-function -Wunused-label -Wunused-variable -Wunused-but-set-variable -Wunused-but-set-parameter")
# # Additional warnings
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Warray-bounds -Wtype-limits -Wreturn-type -Wsequence-point -Wparentheses -Wmissing-braces -Wchar-subscripts -Wswitch -Wwrite-strings -Wenum-compare -Wempty-body -Wlogical-op")

# Check for c++11 support
INCLUDE(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
# CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
# IF(COMPILER_SUPPORTS_CXX11)
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
# ELSEIF(COMPILER_SUPPORTS_CXX0X)
# SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
# ELSE()
#   MESSAGE(ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
# ENDIF()
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O1")

## System dependencies are found with CMake's conventions
# find_package(catkin REQUIRED COMPONENTS
#   roscpp rostime)
# find_package(OpenCV REQUIRED)
find_package(OpenMP)
find_package(cmake_modules)
find_package(Eigen REQUIRED)
find_package(Boost COMPONENTS python serialization)

find_package(PCL REQUIRED)
list(REMOVE_ITEM PCL_LIBRARIES "vtkproj4")


if(OPENMP_FOUND)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()


include_directories(include
  # ${OpenCV_INCLUDE_DIRS}
  ${PCL_INCLUDE_DIRS}
  ${Eigen_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS}
)

link_directories(include ${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

add_library(player_detector src/player_detector.cpp)
target_link_libraries(player_detector
  ${Eigen_LIBRARIES}
  ${PCL_LIBRARIES}
  ${Boost_LIBRARIES}
)




add_executable(assignment src/basketball_players_assignment.cpp src/player_detector.cpp)
target_link_libraries(assignment
  # ${OpenCV_LIBRARIES}
  ${Eigen_LIBRARIES}
  ${PCL_LIBRARIES}
  ${Boost_LIBRARIES}
)
