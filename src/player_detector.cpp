#include <pcl_processor/player_detector.hpp>

PlayerDetector::PlayerDetector () :
debug_(false) , 
expected_nr_people_(13)
{
  cloud_ = CloudType::Ptr (new CloudType);

  cloud_clustered_.clear();

  original_cloud_size_ = 0;
  average_nr_points_per_person_ = 0;


  // TODO: resize the cloud so that people height gets scaled to 
  // a standard measure
  min_person_height_ = 2;
  max_person_height_ = 7;
    
  cloud_loaded_ = false;
  save_clusters_ = false;

  cluster_indices_team_a_.clear();
  cluster_indices_team_b_.clear();
  cluster_indices_referee_.clear();


}


void PlayerDetector::setDebug(bool debug) {
  debug_ = debug;
}

/******************************
*
*
******************************/
bool PlayerDetector::loadCloudXYZRGB (const std::string & file_path)
{

  std::ifstream fs;
  fs.open (file_path.c_str ());
  if (!fs.is_open () || fs.fail ())
  {
    std::cerr << KRED << "Could not open file" << RST << std::endl;
    fs.close ();
    return false;
  }
  
  std::string line;
  std::vector<std::string> st;

  PointType p;
  while (!fs.eof ())
  {
    getline (fs, line);
    // Ignore empty lines
    if (line == "")
      continue;

    // Tokenize the line
    boost::trim (line);
    boost::split (st, line, boost::is_any_of ("\t\r "), boost::token_compress_on);

    if (st.size () != POINTELEMENTS)
      continue;

    
    p.x = float (atof (st[0].c_str ()));
    p.y = float (atof (st[1].c_str ())); 
    p.z = float (atof (st[2].c_str ()));

    // p.r = u_int8 (atoi (st[3].c_str ()));
    // p.g = u_int8 (atoi (st[4].c_str ()));
    // p.b = u_int8 (atoi (st[5].c_str ()));  
    // pack r/g/b into rgb
    uint8_t r = atoi (st[3].c_str ());
    uint8_t g = atoi (st[4].c_str ());
    uint8_t b = atoi (st[5].c_str ());
    // uint8_t g = 0, b = 0;    // Example: Red color
    uint32_t rgb = ((uint32_t)r << 16 | (uint32_t)g << 8 | (uint32_t)b);
    p.rgb = *reinterpret_cast<float*>(&rgb);
    
    // Eigen::Vector3i rgb = p.getRGBVector3i();
    // std::cout << "adding " << p.x << ", " << rgb[0] << ", " << rgb[1] <<
    // ", " << rgb[2]  << "\n"; 
    cloud_->push_back (p);
  }
  fs.close ();

  cloud_->width = uint32_t (cloud_->size ()); 
  cloud_->height = 1; 
  cloud_->is_dense = true;
  
  original_cloud_size_ = cloud_->size();
  return true;
}


/******************************
*
*
******************************/
int PlayerDetector::saveCloudPCD(const std::string & file_path)
{
    pcl::PCDWriter writer;
    int result = writer.write<PointType> (file_path, *cloud_, false);
    return result;
}


bool PlayerDetector::loadCloudPCD(const std::string & file_path)
{
    // load the file 
  if (pcl::io::loadPCDFile<PointType> (file_path, *cloud_) == -1) 
  { 
    PCL_ERROR ("Couldn't read file \n"); 
    return false; 
  } 

  return true;

}



/******************************
*
*
******************************/
int PlayerDetector::clusterExtraction(float tolerance,
                                    int min_cluster_size, int max_cluster_size)
{
  
  // CloudType::Ptr cloud;

  // Creating the KdTree object for the search method of the extraction
  pcl::search::KdTree<PointType>::Ptr tree (new pcl::search::KdTree<PointType>);
  tree->setInputCloud (cloud_);

  // std::vector<pcl::PointIndices> cluster_indices;
  pcl::EuclideanClusterExtraction<PointType> euclidean_cluster;
  euclidean_cluster.setClusterTolerance (1.0f); // 2cm
  euclidean_cluster.setMinClusterSize (50);
  euclidean_cluster.setMaxClusterSize (8000);
  euclidean_cluster.setSearchMethod (tree);
  euclidean_cluster.setInputCloud (cloud_);
  euclidean_cluster.extract (cluster_indices_);

  if (debug_)
    std::cout << "cluster indices size: " << cluster_indices_.size () << std::endl;

  int idx = 0;
  for (std::vector<pcl::PointIndices>::const_iterator it = 
       cluster_indices_.begin (); it != cluster_indices_.end (); ++it)
  {
    CloudType::Ptr cluster_i (new CloudType);
    for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
      cluster_i->points.push_back (cloud_->points[*pit]); //*
    cluster_i->width = cluster_i->points.size ();
    cluster_i->height = 1;
    cluster_i->is_dense = true;

    if (debug_)
      std::cout << "PointCloud representing the Cluster_i: " 
              << cluster_i->points.size () << " data points." << std::endl;
    
    if (save_clusters_) 
    {    
      std::stringstream ss;
      ss << "cluster_" << idx << ".pcd";
      pcd_writer_.write<PointType> (ss.str (), *cluster_i, false); //*
      
    }

    cloud_clustered_.push_back(cluster_i);

    idx++;
}

  return cluster_indices_.size();
}





/******************************
*
*
******************************/
int PlayerDetector::removeNoiseFromCluster() 
{
  int cluster_size = cloud_clustered_.size();
  
  average_nr_points_per_person_ = original_cloud_size_/expected_nr_people_;
  int min_cluster_size = average_nr_points_per_person_/2;
  int max_cluster_size = average_nr_points_per_person_*2;

  if (debug_)
  std::cout << "min & max number of points to control for people : " 
            << min_cluster_size << ", " << max_cluster_size << std::endl;

  std::vector<unsigned int> indices_to_erase;

  for (unsigned int i =0; i< cluster_size; i++)
  {
    if (cloud_clustered_[i]->points.size() < min_cluster_size)
    {
      // remove cluster
      if (debug_)
        std::cout << "removing element number " << i << " with cluster size "
                  << cloud_clustered_[i]->points.size() << std::endl;
      
      indices_to_erase.push_back(i);      

    } else if (cloud_clustered_[i]->points.size() > max_cluster_size)
    {
      // re-cluster
      if (debug_) {
        std::cout << "re-clustering element number " << i << " with cluster size "
                  << cloud_clustered_[i]->points.size() << std::endl;
      }

      // TODO: find a way to re-cluster the large clouds so that each cluster 
      // only contains one person
      std::vector<CloudType::Ptr> new_cluster;
      new_cluster.clear();
      int recluster_size = 0; //clusterExtraction(cloud_clustered_[i], new_cluster, true,0.6f,50);
      if (recluster_size > 1) {
        if (debug_)
          std::cout << "another cluster extracted from the cluster: " 
                    << recluster_size << std::endl;

        indices_to_erase.push_back(i);
      }

    }
  }

  if (debug_)
    std::cout << "indices to erase: " ;

  for (unsigned int i=0; i < indices_to_erase.size(); i++)
  {
    cloud_clustered_.erase(cloud_clustered_.begin()+indices_to_erase[i]);
  

    if (debug_)
      std::cout << indices_to_erase[i] << ", ";
    
    // shift the index to erase as the vector shrinks
    if (i+1 < indices_to_erase.size())
      indices_to_erase[i+1]--;
  }
  

  if (debug_) {
    std::cout << "\n" ;

    for (unsigned int i=0; i < cloud_clustered_.size(); i++)
    {
      std::cout << "cluster: " << i << " size: "
                << cloud_clustered_[i]->points.size () << std::endl;
    }
  }



  return cloud_clustered_.size();
}



/******************************
*
*
******************************/
int PlayerDetector::categorize()
{
  cluster_indices_team_a_.clear();
  cluster_indices_team_b_.clear();
  cluster_indices_referee_.clear();


  std::vector<Eigen::Vector3i> rgb(cloud_clustered_.size());
  std::vector<int> intensity(cloud_clustered_.size());
  int max_color_intensity = -10;
  int min_color_intensity = 300;


  if (debug_)
    std::cout << "intensity: " << std::endl;

  for (int i=0; i < cloud_clustered_.size(); i++)
  {
    
    getPlayerColor(cloud_clustered_[i], rgb[i]);
    intensity[i] = (rgb[i][0]+ rgb[i][1] + rgb[i][2]) / 3;
    
    if (intensity[i] > max_color_intensity)
      max_color_intensity = intensity[i];

    if (intensity[i] < min_color_intensity)
      min_color_intensity = intensity[i];

    if (debug_)
      std::cout << intensity[i] << ", ";

  }

  if (debug_)
    std::cout << "\nmin max intensity: " << min_color_intensity << ", "
              << max_color_intensity << std::endl;


  int intensity_tolerance = 20;

  for (int i=0; i < cloud_clustered_.size(); i++)
  {
    Eigen::Vector3i team_rgb;
    if (intensity[i] > (max_color_intensity - intensity_tolerance))
    {
      // group A
      if (debug_)
        std::cout << "cluster " << i << KRED << " group A " << RST 
                  << "with intensity " << intensity[i] << std::endl;
      team_rgb[0] = 200;
      team_rgb[1] = 10;
      team_rgb[2] = 10;

      cluster_indices_team_a_.push_back(i);


    } else if (intensity[i] < (min_color_intensity + intensity_tolerance))
    {
      // group B
      if (debug_)
        std::cout << "cluster " << i << KGRN << " group B " << RST 
                  << "with intensity " << intensity[i] << std::endl;

      team_rgb[0] = 50;
      team_rgb[1] = 220;
      team_rgb[2] = 50;
      
      cluster_indices_team_b_.push_back(i);
    } else {
      // referee or unknown
      if (debug_)
        std::cout << "cluster " << i << " ref " << RST 
                  << "with intensity " << intensity[i] << std::endl;

      team_rgb[0] = 150;
      team_rgb[1] = 150;
      team_rgb[2] = 150;
      
      cluster_indices_referee_.push_back(i);
    }

    changeCloudColor(cloud_clustered_[i], team_rgb);

  }

  return 0;
}



/******************************
*
*
******************************/
void PlayerDetector::getPlayerColor(const CloudType::Ptr & cloud, 
                                    Eigen::Vector3i & rgb)
{
  CloudType::Ptr cloud_filtered(new CloudType);
  bool negative = false;
  passThroughFilter(cloud, cloud_filtered, min_person_height_, 
                    max_person_height_, negative);

  getColorAverage(cloud_filtered, rgb);
}


/******************************
*
*
******************************/
void PlayerDetector::getColorAverage(const CloudType::Ptr & cloud, 
                                     Eigen::Vector3i & rgb) 
{
  float r,g,b;
  r = g = b = 0.0;

  for (size_t i = 0; i < cloud->points.size(); i++)
  {
      Eigen::Vector3i rgb = cloud->points[i].getRGBVector3i();

      r = r + float(rgb[0])/255.0;
      g = g + float(rgb[1])/255.0;
      b = b + float(rgb[2])/255.0;
  }

  r = r/float(cloud->points.size());
  g = g/float(cloud->points.size());
  b = b/float(cloud->points.size());

  rgb[0] = (int) (r*255.0);
  rgb[1] = (int) (g*255.0);
  rgb[2] = (int) (b*255.0);

  // std::cout << "average rgb: " << (int) (r*255.0) << "\n";
  // std::cout << "average rgb: " << rgb[0] << ", " << rgb[1] << ", "<< rgb[2] << "\n";
}





/******************************
*
*
******************************/
void PlayerDetector::passThroughFilter(const CloudType::Ptr & cloud, 
                       CloudType::Ptr & cloud_filtered,
                       const int min, const int max, const bool negative)
{
  // Create the filtering object
  pcl::PassThrough<PointType> pass;
  pass.setInputCloud (cloud);
  pass.setFilterFieldName ("y");
  pass.setFilterLimits (min, max);
  pass.setFilterLimitsNegative (negative);
  pass.filter (*cloud_filtered);
}





/******************************
*
*
******************************/
void PlayerDetector::changeCloudColor(CloudType::Ptr & cloud, 
                                      const Eigen::Vector3i & rgb)
{
  // pack r/g/b into rgb
  uint32_t rgb_pack = ((uint32_t)rgb[0] << 16 | (uint32_t)rgb[1] 
                      << 8 | (uint32_t)rgb[2]);

  cloud->points[0].rgb = *reinterpret_cast<float*>(&rgb_pack);

  for (unsigned int i=1; i< cloud->points.size() ; i++)
  {
    cloud->points[i].rgb = cloud->points[0].rgb;
  }
}


/******************************
*
*
******************************/
void PlayerDetector::getLocations()
{

  Eigen::Matrix< float, 4, 1 > centroid;
  Eigen::Matrix< float, 2, 1 > location;

  for (int i=0; i < cluster_indices_team_a_.size(); i++) {
    int idx = cluster_indices_team_a_[i];
    int res = pcl::compute3DCentroid ( *cloud_clustered_[idx], centroid );
  
    location[0] = centroid[0]; // x
    location[1] = centroid[2]; // Z

    locations_team_a_.push_back(location);
  }

  for (int i=0; i < cluster_indices_team_b_.size(); i++) {
    int idx = cluster_indices_team_b_[i];
    int res = pcl::compute3DCentroid ( *cloud_clustered_[idx], centroid );
  
    location[0] = centroid[0]; // x
    location[1] = centroid[2]; // Z

    locations_team_b_.push_back(location);
  }
  

  for (int i=0; i < cluster_indices_referee_.size(); i++) {
    int idx = cluster_indices_referee_[i];
    int res = pcl::compute3DCentroid ( *cloud_clustered_[idx], centroid );
  
    location[0] = centroid[0]; // x
    location[1] = centroid[2]; // Z

    locations_referee_.push_back(location);
  }

}




/******************************
*
*
******************************/
void PlayerDetector::printLocations() 
{
  std::cout << KRED << " Team A Locations  " << RST << std::endl;
  std::cout << KRED << "----------------------------------" << RST << std::endl;
  for (int i=0; i < locations_team_a_.size(); i++) {
    std::cout << "[" << locations_team_a_[i][0] << "," 
              << locations_team_a_[i][1] << "]   "; 
  }
  std::cout << "\n" << std::endl;
  std::cout << KGRN << " Team B Locations" << RST << std::endl;
  std::cout << KGRN << "----------------------------------" << RST << std::endl;

  for (int i=0; i < locations_team_b_.size(); i++) {
    std::cout << "[" << locations_team_b_[i][0] << "," 
              << locations_team_b_[i][1] << "]   "; 
  }

  std::cout << "\n" << std::endl;
  std::cout << KBLU << " Referees Locations " << RST << std::endl;
  std::cout << KBLU << "----------------------------------" << RST << std::endl;
  for (int i=0; i < locations_referee_.size(); i++) {
    std::cout << "[" << locations_referee_[i][0] << "," 
              << locations_referee_[i][1] << "]   "; 
  }

  std::cout << "\n" << std::endl;

}


/******************************
*
*
******************************/
VisualizerPtr PlayerDetector::viewCategorizedCloud ()
{
  CloudType::Ptr merged_cloud(new CloudType);
  for (int i=0; i < cloud_clustered_.size(); i++) {
    for (int j=0; j < cloud_clustered_[i]->points.size(); j++)
      merged_cloud->points.push_back(cloud_clustered_[i]->points[j]);
  }

  // --------------------------------------------
  // -----Open 3D viewer and add point cloud-----
  // --------------------------------------------
  VisualizerPtr viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
  viewer->setBackgroundColor (0.1, 0.1, 0.1);
  pcl::visualization::PointCloudColorHandlerRGBField<PointType> rgb(merged_cloud);
  viewer->addPointCloud<PointType> (merged_cloud, rgb, "sample cloud");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "sample cloud");
  // viewer->addCoordinateSystem (1.0);
  viewer->initCameraParameters ();
  return (viewer);
}
