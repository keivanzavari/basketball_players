#include <pcl_processor/player_detector.hpp>

int main(int argc, char* argv[]) {
  std::string dir_path = "../data_files/";  
  std::string file_name = "point_cloud_data.txt";
  std::string file_path;

  if (argc < 2) {
    file_path = dir_path + file_name;
  } else {
    file_path = std::string(argv[1]);
  }

  PlayerDetector detector;

	
  bool result = detector.loadCloudXYZRGB (file_path);
  if (!result) 
  	return -1;

  float tolerance = 1.0f;
  int min_cluster_size = 50;
  int max_cluster_size = 8000;

  int nr_of_clusters = detector.clusterExtraction(tolerance, min_cluster_size,
                                                  max_cluster_size);
  
  int nr_of_clusters_with_people = detector.removeNoiseFromCluster();

  detector.categorize();
  detector.getLocations();
  detector.printLocations();

  VisualizerPtr viewer;
  viewer = detector.viewCategorizedCloud();

  // you can also move this into the class definition provided that the 
  // class will run a separate thread
  while (!viewer->wasStopped ())
  {
    viewer->spinOnce (100);
    boost::this_thread::sleep (boost::posix_time::seconds (1));
  }
	return 0;
}